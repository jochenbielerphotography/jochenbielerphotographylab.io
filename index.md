---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default

services:
  title: Welche Shootings biete ich an?
  subtitle: Shootings
  intro:
  list:
  - title: Bewerbung
    description: Moderne Bewerbungsfotos nach Ihren Wünschen
  - title: Hochzeit
    description: Fotografische Begleitung Ihrer Hochzeit, damit Sie Ihren schönsten Tag immer wieder erleben können
  - title: Portrait
    description: Maßgeschneiderte Portraitfotos nach Ihren Vorstellungen

testimonials:
  title: Das sagen die Leute
  list:
  - name: Simon
    review: Die Bewerbungsfotos entsprechen genau meinen Vorstellungen. Der gesamte Aufnahmeprozess war außerdem sehr angenehm und locker.
  - name: Keshia
    review: Die Bewerbungsfotos gefallen mir ausgesprochen gut! Jochen überzeugt mit einem Auge für Details und seiner sympathischen, kompetenten Art. Für zukünftige Fotos komme ich gerne wieder.
  - name: Katharina
    review: Mit viel Geduld und Experimentierfreude zu einem wirklich tollen Ergebnis gelangt. Jochen ist ein super angenehmer, dezenter und professioneller Fotograf mit einem Auge für Aufnahmen, die sich sehen lassen können! Tolle Nachbearbeitung ins kleinste Detail!

portfolio:
  title: Portfolio
  subtitle: Ein Auszug aus meinen Arbeiten
  list:
    - image: /images/portfolio/DSC02582-5.jpg
    - image: /images/portfolio/DSC02688-2-sw.jpg
    - image: /images/portfolio/DSC02095.jpg
    - image: /images/portfolio/DSC02131.jpg
    - image: /images/portfolio/DSC02311.jpeg
    - image: /images/portfolio/DSC02138.jpeg
    - image: /images/portfolio/DSC02235.jpg
    - image: /images/portfolio/julia-2.jpg
    - image: /images/portfolio/julia.jpg
    - image: /images/portfolio/DSC02089-sw.jpeg
    - image: /images/portfolio/monika.jpg
    - image: /images/portfolio/DSC03986.jpg
    - image: /images/portfolio/DSC04734.jpg
    - image: /images/portfolio/DSC04758.jpg
    - image: /images/portfolio/DSC04759.jpg
    - image: /images/portfolio/DSC04915.jpg
    - image: /images/portfolio/DSC04968.jpg
    - image: /images/portfolio/DSC05584.jpg
    - image: /images/portfolio/DSC05585.jpg

---
